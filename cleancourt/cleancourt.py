import pandas as pd
from difflib import SequenceMatcher
import time
from rapidfuzz import fuzz

from .clean import remove_stopwords, abbreviate, sum_string, calculate_position
from .termdata import abbreviations, stopwords


class RemoveRepetitiveNames():
    '''
    This class is designed to remove repetitive instances of the same party from court data.
    It does this by first preprocessing the party names, including removing punctuation and optionally numbers,
    making all names lowercase, and optionally removing stopwords and abbreviating common terms.
    Second, it matches identical duplicates.
    Third, it uses fuzzy matching to estimate if similar party names are the same party.
    Repeated parties are then combined into the same party.
    '''
    
    def __init__(self, abbreviations=abbreviations, stopwords=stopwords, size=5000,
                 algorithm='levenshtein', remove_numbers=True, score_threshold=0.8,
                 numeric_action='sum', collect_metrics=False, test_subset=.2):
        '''
        Parameters:
        - abbreviations (default None): if the user wants common terms abbreviated, this should be a dict of those abbreviations.
        - stopwords (default None): if the user wants stopwords removed, this should be a list of those stopwords.
        - size (default 10000): only the first 10000 rows are operated on at the moment.
        - algorithm (default 'seq'): two algorithms for fuzzy matching are available:
            - Ratcliff/Obershelp ('seq'): Computes the doubled number of matching characters divided by the total number
                                          of characters in the two strings
            - Levenshtein ('levenshtein'): Computes the minimum number of edits needed to transform one string into the other
        - remove_numbers (default True): if True, removes the numbers from party_name.
        - score_threshold (default .8): the fuzzy matching score required for two party names to be considered duplicates.
        - numeric_action (default 'sum'): how we treat numeric columns for matching party names.
            - 'sum': adds up numeric columns for matching party names.
            - 'avg': averages numeric columns for matching party names.
            - None: treats numeric columns like other columns, adding different numeric entries to a list.
        - collect_metrics (default False): determines if the class will collect the following data for each pair of party names that get compared:
            - party names
            - party name lengths
            - sums of the party name letters
            - party name positions
            - fuzzy matching scores
        - test_subset (default .2): specifies how wide the subset of party names to be compared to a particular party name based on position will be.
            A .2 test_subset would mean that all party names being compared would be within 80% and 120% of the position of the current party name.

        Steps:
        1. Import our data using the provided Pandas DataFrame in self.run().
        2. Select the first self.size (default 10,000) rows.
        3. If self.numeric_action is not None, we split the non-'party_name' columns into numeric and non-numeric columns.
        4. Remove punctuation and optionally numbers from the 'party_name' column.
        5. Normalize whitespace to only be one space.
        6. Make all party names lowercase.
        7. Optionally remove stopwords like llc, inc, and pllc.
        8. Optionally abbreviate common long terms like apartments, company, corporation, etc.
        9. Combine and then drop duplicates in the 'party_name' column.
        10. Calculate 'position' value for every word to reduce the number of comparisons to be made.
        11. Initialize a results dictionary to record the consolidated data as matches are found.
        12. Initialize a 'removed' dictionary to keep track of which party names have been consolidated already.
        13. Iterate over the main DataFrame. If the party name has not already been removed:
            a. Select the potential matches subset of the data in the party name's 'position' range.
            b. Initialize a dictionary to track different column entries for matched party names.
            c. Iterate over the potential matches subset. If the party name has not already been removed:
                i. Calculate the fuzzy matching score between the two party names.
                ii. If the score crosses the self.score_threshold, add the column info to the granular results dictionary and add the party name to the removed dictionary.
            d. Add the recorded column info to the overall results dictionary.
        14. Create an output DataFrame from the overall results dictionary.
        '''
        self.abbreviations = abbreviations
        self.stopwords = stopwords
        self.algorithm = algorithm
        self.remove_numbers = remove_numbers
        self.size = size
        self.score_threshold = score_threshold
        self.sum_string = sum_string
        self.abbreviate = abbreviate
        self.remove_stopwords = remove_stopwords
        self.calculate_position = calculate_position
        self.numeric_action = numeric_action
        self.collect_metrics = collect_metrics
        self.test_subset = test_subset

        if self.collect_metrics:
            self.metrics = {
                'party_name_1': [],
                'party_name_2': [],
                'word_length_1': [],
                'word_length_2': [],
                'word_sum_1': [],
                'word_sum_2': [],
                'position_1': [],
                'position_2': [],
                'fuzzy_matching_score': []
            }

    def run(self, df: pd.DataFrame):
        # TIME CHECK MODULE
        start_time = time.time()
        print('timer started')
        self.data = df[:self.size]

        # Parameterize columns
        # We may perform sum or avg operations on numeric columns
        # so we want to keep them separate from other columns
        num_columns = []
        other_columns = []

        column_types = self.data.dtypes # Get datatypes of the columns
        if self.numeric_action:
            for column_name, data_type in column_types.iteritems(): # Iterate over the datatypes
                if column_name != 'party_name': # exclude party_name column
                    if 'int' in str(data_type) or 'float' in str(data_type):
                        if column_name != 'Unnamed: 0':
                            num_columns.append(column_name)
                    else:
                        other_columns.append(column_name)
        else: # If no numeric_action is specified, we treat all columns the same
            other_columns = [column for column in self.data.columns if column != 'party_name' and column != 'Unnamed: 0']

        # remove punctuation and optionally numbers
        self.data = self.remove_punc_num(self.data)
        # make sure all whitespace is only one space
        self.data.party_name = self.data.party_name.replace('\s+', ' ', regex=True)
        # lowercase all party names
        self.data['party_name'] = self.data['party_name'].str.lower()

        # optionally remove stopwords like llc, inc, and pllc
        if self.stopwords:
            self.data['party_name'] = self.data['party_name'].apply(lambda x:
                                                                    self.remove_stopwords(x, self.stopwords))
        # optionally abbreviate common long terms like apartments, company, corporation, etc.
        if self.abbreviations:
            self.data['party_name'] = self.data['party_name'].apply(lambda x:
                                                                    self.abbreviate(x, self.abbreviations))
        # combine and then drop duplicates
        self.data[num_columns] = self.data.groupby(['party_name'])[num_columns].transform(self.numeric_action)
        self.data.drop_duplicates(subset=['party_name'], inplace=True)
        # record how many identical party_names were identified, combined, and removed
        new_size, _ = self.data.shape
        self.dropped_duplicates_count = self.size - new_size
        # TIME CHECK MODULE
        first_interval = time.time()
        print('first interval: ', first_interval - start_time)

        # calculate 'position' value for every word to reduce the number of comparisons to be made.
        word_lengths = []
        word_sums = []
        position = []

        self.data['word_length'] = self.data['party_name'].apply(lambda party_name: len(party_name))
        self.data['word_sum'] = self.data['party_name'].apply(lambda party_name:
                                                              self.sum_string(party_name))
        self.data['position'] = self.data['party_name'].apply(lambda party_name:
                                                                 self.calculate_position(party_name))

        # TIME CHECK MODULE
        second_interval = time.time()
        print('second interval: ', second_interval - first_interval)

        # The following dictionaries will be used to record data from all non-'party_name' columns
        if num_columns:
            num_columns_overall_results = {}
            for column in num_columns:
                num_columns_overall_results[column] = []
        if other_columns:
            other_columns_overall_results = {}
            for column in other_columns:
                other_columns_overall_results[column] = []

        # We also record the party names and their aliases
        party_names = []
        aliases = []
        
        # Keep track of which party names have already been removed using a dictionary
        indices = self.data.index
        values = [False] * len(indices)
        removed = dict(zip(indices, values))

        count = 0
        interval = time.time()

        # For testing purposes: tracking how many parties get dropped
        self.starting_count, _ = self.data.shape
        self.fuzzy_match_count = 0

        # Iterating over all party names
        for index, row in self.data.iterrows():
            # TIME CHECK MODULE
            count += 1
            if count%1000 == 0:
                print('finished thousand rows: ', time.time() - interval)
                interval = time.time()
            
            if removed[index] == False: # If the word has not yet been visited or consolidating
                removed[index] = True # The word has now been visited

                # Get a subset of potential matches based on position, which is a combo of name length and letters
                position_lower = row['position'] * (1-self.test_subset)
                position_upper = row['position'] * (1+self.test_subset)
                test_set = self.data[(self.data['position']<=position_upper) & (self.data['position']>=position_lower)]
                
                if num_columns: # Data from numeric columns are summed or averaged
                    num_columns_granular_results = {}
                    for column in num_columns:
                        num_columns_granular_results[column] = 0

                if other_columns: # Data from other columns are simply recorded in a list
                    other_columns_granular_results = {}
                    for column in other_columns:
                        if not pd.isna(row[column]):
                            other_columns_granular_results[column] = [row[column]]
                        else:
                            other_columns_granular_results[column] = []

                party_names.append(row['party_name'])
                party_aliases = []
                
                names_count = 0

                # Now we iterate over the potential matches subset
                for index_2, row_2 in test_set.iterrows():
                    if removed[index_2] == False: # If the word has not been visited or consolidated
                        if self.algorithm == 'seq':
                            score = self.seq(row['party_name'], row_2['party_name'])
                        elif self.algorithm == 'levenshtein':
                            score = self.levenshtein(row['party_name'], row_2['party_name'])/100
                        else:
                            score = self.seq(row['party_name'], row_2['party_name'])
                        if score >= self.score_threshold:
                            if self.collect_metrics:
                                self.gather_metrics(row, row_2, score)

                            names_count += 1 # Count of matches for averaging numeric column results

                            self.fuzzy_match_count += 1
                            removed[index_2] = True # The word has now been consolidated

                            if other_columns: # Record data from other columns
                                for column in other_columns:
                                    if not pd.isna(row_2[column]) and row_2[column] not in other_columns_granular_results[column]:
                                        other_columns_granular_results[column].append(row_2[column])
                            if num_columns: # Record data from numeric columns
                                for column in num_columns:
                                    num_columns_granular_results[column] += row_2[column]
                            if row_2['party_name'] not in party_aliases: # Keep track of the different party names that were combined
                                party_aliases.append(row_2['party_name'])

                aliases.append(party_aliases)
                if num_columns:
                    for column in num_columns:
                        if self.numeric_action == 'sum': # We already summed up the data during collection
                            result = num_columns_granular_results[column]
                        elif self.numeric_action == 'avg': # We need to divide the summed up data by the number of data points
                            result = num_columns_granular_results[column]/names_count
                        num_columns_overall_results[column].append(result)
                if other_columns:
                    for column in other_columns: # Adding granular results to the overall results dictionary
                        other_columns_overall_results[column].append(other_columns_granular_results[column])

        # TIME CHECK MODULE
        end_interval = time.time()
        print('finished main function: ', end_interval - start_time)
        print('number of matches: ', self.fuzzy_match_count)
        
        # Put together the full results dictionary
        full_results_dict = {}
        full_results_dict['party_name'] = party_names
        full_results_dict['aliases'] = aliases
        if other_columns:
            full_results_dict.update(other_columns_overall_results)
        if num_columns:
            full_results_dict.update(num_columns_overall_results)

        output_df = pd.DataFrame(full_results_dict)
        return output_df

    def remove_punc_num(self, df):
        '''
        This code was taken from https://stackoverflow.com/questions/50444346/fast-punctuation-removal-with-pandas
        It removes punctuation and optionally numbers.
        '''
        if self.remove_numbers == True: # numbers are included in the text to be removed.
            punct = '1234567890!"#$%&\'()*+,-./:;<=>?@[\\]^_`{}~'
        else: # only punctuation is included in the text to be removed.
            punct = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{}~'

        transtab = str.maketrans(dict.fromkeys(punct, ''))

        df['party_name'] = '|'.join(df['party_name'].tolist()).translate(transtab).split('|')

        return df
    
    def seq(self, a, b):
        '''
        Returns similarity ratio for two party names using the Ratcliff/Obershelp algorithm.
        '''
        return SequenceMatcher(None, a, b).ratio()
    
    def levenshtein(self, a, b):
        '''
        Returns similarity ratio for two party names using the Levenshtein algorithm.
        '''
        return fuzz.ratio(a, b)
    
    def gather_metrics(self, row_1, row_2, score):
        self.metrics['party_name_1'].append(row_1['party_name'])
        self.metrics['party_name_2'].append(row_2['party_name'])
        self.metrics['word_length_1'].append(row_1['word_length'])
        self.metrics['word_length_2'].append(row_2['word_length'])
        self.metrics['word_sum_1'].append(row_1['word_sum'])
        self.metrics['word_sum_2'].append(row_2['word_sum'])
        self.metrics['position_1'].append(row_1['position'])
        self.metrics['position_2'].append(row_2['position'])
        self.metrics['fuzzy_matching_score'].append(score)