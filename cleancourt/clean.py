def remove_stopwords(party_name, stopwords):
    '''
    This method removes common stopwords such as llc, pllc, and inc.
    Removing these stopwords increases the number of duplicate party names and can also improve fuzzy matching.
    '''
    items = party_name.split(' ')

    new_party_name_list = []

    for item in items:
        if item not in stopwords:
            new_party_name_list.append(item)
        else:
            continue

    delimiter = ' '
    new_party_name = delimiter.join(new_party_name_list)

    return new_party_name


def abbreviate(party_name, abbreviations):
    '''
    This method abbreviates common terms such as apartments, corporation, company, etc.
    By abbreviating these large terms, false positive are reduced because terms that might
    be shared by two unrelated parties carry less weight.
    '''
    items = party_name.split(' ')

    new_party_name_list = []

    for item in items:
        if item in abbreviations:
            new_party_name_list.append(abbreviations[item])
        else:
            new_party_name_list.append(item)

    delimiter = ' '
    new_party_name = delimiter.join(new_party_name_list).strip()

    return new_party_name


def sum_string(string):
    '''
    Converts letters to numbers (a=1, b=2, etc.) and returns the sum of all letters in a string.
    '''
    return sum(ord(c) - 64 for c in string)


def calculate_position(word):
    word_length = len(word)
    word_sum = sum_string(word)
    word_position = word_sum + word_length*33
    return word_position