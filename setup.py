#!/usr/bin/env python

from setuptools import setup


setup(name='cleancourt',
      description='Python library to process names in court records',
      version='0.1',
      license="MIT",
      packages=["cleancourt"],
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
)