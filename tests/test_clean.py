from cleancourt.clean import remove_stopwords, abbreviate
from cleancourt.termdata import stopwords, abbreviations


def test_clean():
    input = "the building llc"
    expected_output = "the building"

    output = remove_stopwords(input, stopwords=stopwords)
    assert output == expected_output


def test_abbreviate():
    input = "the building apartments"
    expected_output = "the building apts"

    output = abbreviate(input, abbreviations=abbreviations)
    assert output == expected_output
