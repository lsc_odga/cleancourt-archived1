from cleancourt.cleancourt import RemoveRepetitiveNames
import pandas as pd
import os
import time

def test_RemoveRepetitiveNames_fakedata():
    # Get our test dataset
    this_dir, this_filename = os.path.split(__file__)
    DATA_PATH = os.path.join(this_dir, 'input', 'fake_data.csv')
    data = pd.read_csv(DATA_PATH)
    data.reset_index(inplace=True, drop=True)

    model = RemoveRepetitiveNames()
    result = model.run(data)

    timestr = time.strftime("%Y%m%d-%H%M%S")
    test_destination = os.path.join(this_dir, 'output', 'test_' + timestr + '.csv')
    result.to_csv(test_destination, index=False)

    current_size, _ = result.shape
    ideal_size = model.starting_count - model.fuzzy_match_count
    assert current_size == ideal_size
